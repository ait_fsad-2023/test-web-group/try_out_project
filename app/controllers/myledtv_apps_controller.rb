class MyledtvAppsController < ApplicationController
  before_action :set_myledtv_app, only: %i[ show edit update destroy ]

  # GET /myledtv_apps or /myledtv_apps.json
  def index
    @myledtv_apps = MyledtvApp.all
  end

  # GET /myledtv_apps/1 or /myledtv_apps/1.json
  def show
  end

  # GET /myledtv_apps/new
  def new
    @myledtv_app = MyledtvApp.new
  end

  # GET /myledtv_apps/1/edit
  def edit
  end

  # POST /myledtv_apps or /myledtv_apps.json
  def create
    @myledtv_app = MyledtvApp.new(myledtv_app_params)

    respond_to do |format|
      if @myledtv_app.save
        format.html { redirect_to myledtv_app_url(@myledtv_app), notice: "Myledtv app was successfully created." }
        format.json { render :show, status: :created, location: @myledtv_app }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @myledtv_app.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /myledtv_apps/1 or /myledtv_apps/1.json
  def update
    respond_to do |format|
      if @myledtv_app.update(myledtv_app_params)
        format.html { redirect_to myledtv_app_url(@myledtv_app), notice: "Myledtv app was successfully updated." }
        format.json { render :show, status: :ok, location: @myledtv_app }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @myledtv_app.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /myledtv_apps/1 or /myledtv_apps/1.json
  def destroy
    @myledtv_app.destroy

    respond_to do |format|
      format.html { redirect_to myledtv_apps_url, notice: "Myledtv app was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_myledtv_app
      @myledtv_app = MyledtvApp.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def myledtv_app_params
      params.require(:myledtv_app).permit(:name, :facts)
    end
end
