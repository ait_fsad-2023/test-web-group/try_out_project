require "test_helper"

class MyledtvAppsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @myledtv_app = myledtv_apps(:one)
  end

  test "should get index" do
    get myledtv_apps_url
    assert_response :success
  end

  test "should get new" do
    get new_myledtv_app_url
    assert_response :success
  end

  test "should create myledtv_app" do
    assert_difference("MyledtvApp.count") do
      post myledtv_apps_url, params: { myledtv_app: { facts: @myledtv_app.facts, name: @myledtv_app.name } }
    end

    assert_redirected_to myledtv_app_url(MyledtvApp.last)
  end

  test "should show myledtv_app" do
    get myledtv_app_url(@myledtv_app)
    assert_response :success
  end

  test "should get edit" do
    get edit_myledtv_app_url(@myledtv_app)
    assert_response :success
  end

  test "should update myledtv_app" do
    patch myledtv_app_url(@myledtv_app), params: { myledtv_app: { facts: @myledtv_app.facts, name: @myledtv_app.name } }
    assert_redirected_to myledtv_app_url(@myledtv_app)
  end

  test "should destroy myledtv_app" do
    assert_difference("MyledtvApp.count", -1) do
      delete myledtv_app_url(@myledtv_app)
    end

    assert_redirected_to myledtv_apps_url
  end
end
