require "application_system_test_case"

class MyledtvAppsTest < ApplicationSystemTestCase
  setup do
    @myledtv_app = myledtv_apps(:one)
  end

  test "visiting the index" do
    visit myledtv_apps_url
    assert_selector "h1", text: "Myledtv apps"
  end

  test "should create myledtv app" do
    visit myledtv_apps_url
    click_on "New myledtv app"

    fill_in "Facts", with: @myledtv_app.facts
    fill_in "Name", with: @myledtv_app.name
    click_on "Create Myledtv app"

    assert_text "Myledtv app was successfully created"
    click_on "Back"
  end

  test "should update Myledtv app" do
    visit myledtv_app_url(@myledtv_app)
    click_on "Edit this myledtv app", match: :first

    fill_in "Facts", with: @myledtv_app.facts
    fill_in "Name", with: @myledtv_app.name
    click_on "Update Myledtv app"

    assert_text "Myledtv app was successfully updated"
    click_on "Back"
  end

  test "should destroy Myledtv app" do
    visit myledtv_app_url(@myledtv_app)
    click_on "Destroy this myledtv app", match: :first

    assert_text "Myledtv app was successfully destroyed"
  end
end
