json.extract! myledtv_app, :id, :name, :facts, :created_at, :updated_at
json.url myledtv_app_url(myledtv_app, format: :json)
